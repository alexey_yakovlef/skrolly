package com.skosc.skrolly.feature.scroller

import com.skosc.skrolly.lib.mvvm.bindVm
import com.skosc.skrolly.lib.mvvm.viewModelProvider
import com.skosc.skrolly.lib.di.DefaultModule
import org.kodein.di.generic.instance

val ModuleFeatureScroll = DefaultModule("feature-scroll") {
    bindVm(ScrollViewModel::class) with viewModelProvider { ScrollViewModelImpl(instance()) }
}