package com.skosc.skrolly.feature.scroller

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.skosc.skrolly.lib.mvvm.BaseViewModel
import com.skosc.skrolly.lib.mvvm.coroutines.launchMediatorLiveData
import com.skosc.skrolly.domain.images.entitiy.Image
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import com.skosc.skrolly.domain.images.entitiy.ProviderNodeId
import com.skosc.skrolly.domain.images.model.ImagesModel

abstract class ScrollViewModel : BaseViewModel() {
    abstract fun images(providerType: ImageProviderType, nodeId: ProviderNodeId): LiveData<PagedList<Image>>
}

class ScrollViewModelImpl(private val imagesModel: ImagesModel) : ScrollViewModel() {
    override fun images(providerType: ImageProviderType, nodeId: ProviderNodeId): LiveData<PagedList<Image>> =
        launchMediatorLiveData {
            imagesModel.makePagedImagesList(providerType, nodeId)
        }
}