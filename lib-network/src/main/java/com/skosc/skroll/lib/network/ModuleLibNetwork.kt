package com.skosc.skroll.lib.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.skosc.skrolly.lib.di.DefaultModule
import okhttp3.Interceptor
import org.kodein.di.fullErasedName
import org.kodein.di.generic.*
import java.lang.reflect.Type

val ModuleLibNetwork = DefaultModule("lib-network") {
    bind() from setBinding<Interceptor>()
    bind() from setBinding<Pair<Type, JsonDeserializer<*>>>()
    bind<Gson>() with singleton {
        val adapters = instance<Set<Pair<Type, JsonDeserializer<*>>>>()

        GsonBuilder()
            .apply { adapters.forEach { (type, adapter) -> registerTypeHierarchyAdapter(Class.forName(type.fullErasedName()), adapter) } }
            .create()
    }
}
