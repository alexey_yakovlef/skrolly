package com.skosc.skroll.lib.network

import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.reflect.KClass

open class WebApiClient(private val gson: Gson, private val interceptors: Set<Interceptor>) {

    fun <T: Any> createService(serviceClass: KClass<T>): T {
        return createRetrofit().create(serviceClass.java)
    }

    private fun createRetrofit(): Retrofit {
        return createBaseRetrofitBuilder().build()
    }

    protected open fun createBaseRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
            .client(createHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    protected open fun createHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .apply { interceptors.forEach { addNetworkInterceptor(it) } }
            .build()
    }
}