package com.skosc.skrolly.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class SkrollyPlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        project.pluginManager.apply("kotlin-android")
        project.pluginManager.apply("kotlin-android-extensions")

        project.android {
            compileSdkVersion 29
            buildToolsVersion "29.0.0"
            defaultConfig {
                minSdkVersion 21
                targetSdkVersion 29
                versionCode 1
                versionName "1.0"
                testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
            }

            buildTypes {
                release {
                    minifyEnabled false
                }

                ci {

                }
            }

            lintOptions {
                checkReleaseBuilds false
                abortOnError false
            }
        }

        project.dependencies {
            implementation Dependencies.TIMBER
        }
    }
}