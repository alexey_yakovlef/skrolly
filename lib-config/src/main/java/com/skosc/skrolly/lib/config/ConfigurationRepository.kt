package com.skosc.skrolly.lib.config

import java.lang.reflect.Type

interface ConfigurationRepository {
    suspend fun <T : Any> get(resource: String, type: Type): T
}
