package com.skosc.skrolly.domain.images.repo

import com.google.gson.reflect.TypeToken
import com.skosc.skrolly.domain.images.entitiy.ImageProvider
import com.skosc.skrolly.domain.images.entitiy.ImageProviderNode
import com.skosc.skrolly.lib.config.ConfigurationRepository

class ImageProviderRepository(private val configRepo: ConfigurationRepository) {
    companion object {
        private const val KEY_PROVIDERS_CONFIG = "providers"
    }

    suspend fun loadProviders(): List<ImageProvider<ImageProviderNode>> {
        return configRepo.get(
            KEY_PROVIDERS_CONFIG,
            object : TypeToken<List<ImageProvider<ImageProviderNode>>>() {}.type
        )
    }
}