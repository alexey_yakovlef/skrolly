package com.skosc.skrolly.domain.images.model

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.skosc.skrolly.domain.images.ImagePagedDataSourceContainer
import com.skosc.skrolly.domain.images.entitiy.Image
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import com.skosc.skrolly.domain.images.entitiy.ProviderNodeId

class ImagesModel(private val imageSourceContainer: ImagePagedDataSourceContainer) {
    suspend fun makePagedImagesList(providerType: ImageProviderType, node: ProviderNodeId): LiveData<PagedList<Image>> {
        return imageSourceContainer.forType(providerType)
            .createImagePagedList(node)
    }
}