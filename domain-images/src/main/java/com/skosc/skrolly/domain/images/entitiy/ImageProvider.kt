package com.skosc.skrolly.domain.images.entitiy

import com.google.gson.annotations.SerializedName

typealias ProviderNodeId = String

enum class ImageProviderType {
    @SerializedName("reddit") REDDIT
}

data class ImageProvider<out Node: ImageProviderNode>(
    @SerializedName("type") val type: ImageProviderType,
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("nodes") val nodes: List<Node>
)