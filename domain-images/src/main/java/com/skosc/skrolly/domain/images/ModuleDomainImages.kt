package com.skosc.skrolly.domain.images

import com.google.gson.JsonDeserializer
import com.google.gson.reflect.TypeToken
import com.skosc.skrolly.core.typeOf
import com.skosc.skrolly.domain.images.entitiy.ImageProvider
import com.skosc.skrolly.domain.images.entitiy.ImageProviderNode
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import com.skosc.skrolly.domain.images.misc.ImageProviderDeserializer
import com.skosc.skrolly.domain.images.misc.ImageProviderNodeDeserializer
import com.skosc.skrolly.domain.images.misc.ImageProviderNodeDeserializerPack
import com.skosc.skrolly.domain.images.model.ImagesModel
import com.skosc.skrolly.domain.images.repo.ImageProviderRepository
import com.skosc.skrolly.lib.di.DefaultModule
import org.kodein.di.generic.*
import java.lang.reflect.Type

val ModuleDomainImages = DefaultModule("domain-images") {
    bind() from setBinding<ImagePagedDataSource>()
    bind() from setBinding<ImageProviderNodeDeserializerPack>()
    bind<ImagePagedDataSourceContainer>() with singleton { ImagePagedDataSourceContainer(instance()) }
    bind<ImagesModel>() with singleton { ImagesModel(instance()) }
    bind<ImageProviderRepository>() with singleton { ImageProviderRepository(instance()) }
    bind<Pair<Type, JsonDeserializer<*>>>().inSet() with singleton {
        val nodeSerializers = instance<Set<ImageProviderNodeDeserializerPack>>().toMap()
        typeOf<ImageProvider<*>>() to ImageProviderDeserializer(nodeSerializers)
    }
}