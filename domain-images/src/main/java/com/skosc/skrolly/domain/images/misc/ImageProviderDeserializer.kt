package com.skosc.skrolly.domain.images.misc

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.skosc.skrolly.core.typeOf
import com.skosc.skrolly.domain.images.entitiy.ImageProvider
import com.skosc.skrolly.domain.images.entitiy.ImageProviderNode
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import java.lang.reflect.Type

class ImageProviderDeserializer(
    private val nodeSerializers: Map<ImageProviderType, ImageProviderNodeDeserializer>
) : JsonDeserializer<ImageProvider<ImageProviderNode>> {
    companion object {
        private const val KEY_ID = "id"
        private const val KEY_TITLE = "title"
        private const val KEY_TYPE = "type"
        private const val KEY_NODES = "nodes"
    }

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): ImageProvider<ImageProviderNode> {
        val jsonObject = json.asJsonObject
        val id = jsonObject[KEY_ID].asString
        val title = jsonObject[KEY_TITLE].asString

        val type = context.deserialize<ImageProviderType>(jsonObject[KEY_TYPE], typeOf<ImageProviderType>())
        val nodeDeserializer = nodeSerializers[type]
            ?: error("Can't find node deserializer for provider type $type")

        val jsonArray = jsonObject[KEY_NODES].asJsonArray
        val nodes = jsonArray.map { nodeJson -> nodeDeserializer.deserialize(nodeJson, nodeDeserializer.type, context) }

        return ImageProvider(id = id, title = title, type = type, nodes = nodes)
    }

}
