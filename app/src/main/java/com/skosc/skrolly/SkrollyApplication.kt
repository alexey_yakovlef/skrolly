package com.skosc.skrolly

import android.app.Application
import com.skosc.skroll.lib.network.ModuleLibNetwork
import com.skosc.skrolly.debug.ModuleLibDebug
import com.skosc.skrolly.domain.images.ModuleDomainImages
import com.skosc.skrolly.domain.reddit.ModuleDomainReddit
import com.skosc.skrolly.feature.menu.ModuleFeatureMenu
import com.skosc.skrolly.feature.scroller.ModuleFeatureScroll
import com.skosc.skrolly.lib.android.ApplicationPlugin
import com.skosc.skrolly.lib.di.imports
import com.skosc.skrolly.lib.firebase.ModuleLibFirebase
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class SkrollyApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        importOnce(ModuleApplication)
        importOnce(ModuleLibNetwork)
        imports(ModuleDomainImages) {
            importOnce(ModuleDomainReddit)
        }
        importOnce(ModuleFeatureScroll)
        importOnce(ModuleFeatureMenu)
        importOnce(ModuleLibDebug)
        importOnce(ModuleLibFirebase)
    }

    private val plugins: Set<ApplicationPlugin> by instance()

    override fun onCreate() {
        super.onCreate()
        plugins.forEach { it.onCreate(this) }
    }

    override fun onTerminate() {
        super.onTerminate()
        plugins.forEach { it.onTerminate(this) }
    }
}