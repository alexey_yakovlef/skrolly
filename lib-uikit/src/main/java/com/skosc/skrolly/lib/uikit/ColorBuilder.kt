package com.skosc.skrolly.lib.uikit

import android.graphics.Color
import kotlin.math.min
import kotlin.math.round


@Suppress("NOTHING_TO_INLINE")
inline class ColorBuilder(val current: Int) {
    companion object {
        const val EXCEPT_R_MASK = -0xff0001
        const val ONLY_R_MASK = EXCEPT_R_MASK.inv()
        const val EXCEPT_B_MASK = -0x100
        const val ONLY_B_MASK = EXCEPT_B_MASK.inv()
    }

    inline fun factor(factor: Float): ColorBuilder {
        val alpha = Color.alpha(current)
        val red: Int = round(Color.red(current).toDouble() * factor).toInt()
        val green: Int = round(Color.green(current).toDouble() * factor).toInt()
        val blue: Int = round(Color.blue(current).toDouble() * factor).toInt()
        val argb = Color.argb(alpha, min(red, 255), min(green, 255), min(blue, 255))
        return ColorBuilder(argb)
    }

    inline fun invert(): ColorBuilder {
        val red = current and ONLY_R_MASK shr 16
        val blue = current and ONLY_B_MASK
        return ColorBuilder(current and EXCEPT_R_MASK and EXCEPT_B_MASK or (blue shl 16) or red)
    }

    inline fun build(): Int = current
}

fun Int.manipulateColor(): ColorBuilder = ColorBuilder(this)