package com.skosc.skrolly.lib.android

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil

class SimpleDiffCallback<T : Any> : DiffUtil.ItemCallback<T>() {

    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem === newItem

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem == newItem

}