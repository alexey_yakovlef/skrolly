package com.skosc.skrolly.lib.firebase

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.skosc.skrolly.lib.android.ApplicationPlugin
import com.skosc.skrolly.lib.config.ConfigurationRepository
import com.skosc.skrolly.lib.di.DefaultModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.inSet
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val ModuleLibFirebase = DefaultModule("lib-firebase") {
    bind<ApplicationPlugin>().inSet() with singleton { FirebaseApplicationPlugin() }
    bind<FirebaseRemoteConfig>() with singleton { FirebaseRemoteConfig.getInstance() }
    bind<ConfigurationRepository>() with singleton { FirebaseConfigRepository(instance(), instance()) }
}