package com.skosc.skrolly.lib.di

import org.kodein.di.Kodein

private const val DEFAULT_KODEIN_MODULE_PREFIX = "sk_dfm_"
private const val DEFAULT_KODEIN_SUBMODULE_PREFIX = "sk_sub_dfm_"

fun DefaultModule(name: String, builder: Kodein.Builder.() -> Unit): Kodein.Module {
  return Kodein.Module(
          name = name,
          allowSilentOverride = false,
          prefix = DEFAULT_KODEIN_MODULE_PREFIX,
          init = builder
  )
}


/**
 * Imports passed [module] and then imports new [DefaultModule] build with [subModuleBuilder]
 */
fun Kodein.Builder.imports(module: Kodein.Module, subModuleBuilder: Kodein.Builder.() -> Unit) {
    importOnce(module)
    importOnce(DefaultModule("$DEFAULT_KODEIN_SUBMODULE_PREFIX${module.name}", subModuleBuilder))
}