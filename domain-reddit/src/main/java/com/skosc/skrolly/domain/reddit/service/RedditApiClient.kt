package com.skosc.skrolly.domain.reddit.service

import com.google.gson.Gson
import com.skosc.skroll.lib.network.WebApiClient
import okhttp3.Interceptor
import retrofit2.Retrofit

class RedditApiClient(gson: Gson, interceptors: Set<Interceptor>) : WebApiClient(gson, interceptors) {
    override fun createBaseRetrofitBuilder(): Retrofit.Builder {
        return super.createBaseRetrofitBuilder()
            .baseUrl("https://www.reddit.com")
    }
}