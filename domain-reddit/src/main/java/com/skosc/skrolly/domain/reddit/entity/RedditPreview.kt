package com.skosc.skrolly.domain.reddit.entity

import com.google.gson.annotations.SerializedName

data class RedditPreview(
    @SerializedName("images") val images: List<RedditImagePack>?
)