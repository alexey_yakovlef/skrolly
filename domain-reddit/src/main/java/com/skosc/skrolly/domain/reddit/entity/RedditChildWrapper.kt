package com.skosc.skrolly.domain.reddit.entity

import com.google.gson.annotations.SerializedName

data class RedditChildWrapper<T>(
    @SerializedName("kind") val kind: String,
    @SerializedName("data") val data: T
)