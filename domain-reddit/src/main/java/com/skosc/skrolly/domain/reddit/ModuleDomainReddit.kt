package com.skosc.skrolly.domain.reddit

import com.skosc.skrolly.domain.images.ImagePagedDataSource
import com.skosc.skrolly.domain.images.entitiy.ImageProviderType
import com.skosc.skrolly.domain.images.misc.ImageProviderNodeDeserializer
import com.skosc.skrolly.domain.images.misc.ImageProviderNodeDeserializerPack
import com.skosc.skrolly.domain.reddit.misc.RedditImageProviderNodeDeserializer
import com.skosc.skrolly.domain.reddit.model.RedditImageModel
import com.skosc.skrolly.domain.reddit.repository.RedditPostRepository
import com.skosc.skrolly.domain.reddit.service.RedditApiClient
import com.skosc.skrolly.domain.reddit.service.RedditPostService
import com.skosc.skrolly.lib.di.DefaultModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.inSet
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val ModuleDomainReddit = DefaultModule("domain-reddit") {
    bind<RedditApiClient>() with singleton {
        RedditApiClient(instance(), instance())
    }
    bind<RedditPostService>() with singleton {
        instance<RedditApiClient>()
            .createService(RedditPostService::class)
    }
    bind<RedditPostRepository>() with singleton { RedditPostRepository(instance()) }
    bind<RedditImageModel>() with singleton { RedditImageModel(instance()) }

    bind<ImagePagedDataSource>().inSet() with singleton { instance<RedditImageModel>() }
    bind<ImageProviderNodeDeserializerPack>().inSet() with singleton {
        ImageProviderType.REDDIT to RedditImageProviderNodeDeserializer()
    }
}
