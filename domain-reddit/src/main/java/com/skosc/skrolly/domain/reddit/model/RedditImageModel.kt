package com.skosc.skrolly.domain.reddit.model

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.skosc.skrolly.domain.images.ImagePagedDataSource
import com.skosc.skrolly.domain.images.entitiy.*
import com.skosc.skrolly.domain.reddit.entity.RedditImage
import com.skosc.skrolly.domain.reddit.entity.RedditPost
import com.skosc.skrolly.domain.reddit.repository.RedditPostRepository
import kotlinx.coroutines.coroutineScope

class RedditImageModel(private val redditPostRepository: RedditPostRepository) : ImagePagedDataSource {
    companion object {
        private const val PAGE_SIZE = 25
    }

    override val assignedImageProviderType: ImageProviderType = ImageProviderType.REDDIT

    override suspend fun createImagePagedList(node: ProviderNodeId): LiveData<PagedList<Image>> {
        return createRedditPostDataFactory(node).mapByPage {
            it.flatMap { post ->
                post.preview?.images?.map { imagePack -> imagePack.source as Image } ?: emptyList()
            }
        }.toLiveData(PAGE_SIZE)
    }

    private suspend fun createRedditPostDataFactory(subReddit: String): DataSource.Factory<String, RedditPost> {
        return coroutineScope {
            return@coroutineScope object : DataSource.Factory<String, RedditPost>() {
                override fun create(): DataSource<String, RedditPost> {
                    return RedditPostDataSource(this@coroutineScope, redditPostRepository, subReddit)
                }
            }
        }
    }
}