package com.skosc.skrolly.feature.menu

import com.skosc.skrolly.lib.di.DefaultModule
import com.skosc.skrolly.lib.mvvm.bindVm
import com.skosc.skrolly.lib.mvvm.viewModelProvider
import org.kodein.di.generic.instance

val ModuleFeatureMenu = DefaultModule("feature-menu") {
    bindVm(MenuViewModel::class) with viewModelProvider { MenuViewModelImpl(instance()) }
}