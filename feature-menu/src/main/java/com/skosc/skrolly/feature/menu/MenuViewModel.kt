package com.skosc.skrolly.feature.menu

import androidx.lifecycle.LiveData
import com.skosc.skrolly.domain.images.repo.ImageProviderRepository
import com.skosc.skrolly.lib.mvvm.BaseViewModel
import com.skosc.skrolly.lib.mvvm.coroutines.launchLiveDataDelgate

abstract class MenuViewModel : BaseViewModel() {
    abstract val menuItems: LiveData<List<MenuItemUiObject>>
}

class MenuViewModelImpl(providerRepository: ImageProviderRepository) : MenuViewModel() {

    override val menuItems: LiveData<List<MenuItemUiObject>> by launchLiveDataDelgate(this) {
        providerRepository.loadProviders()
            .flatMap { provider ->
                provider.nodes.map { node ->
                    MenuItemUiObject(node.title, provider.type, node.id)
                }
            }
    }

}
