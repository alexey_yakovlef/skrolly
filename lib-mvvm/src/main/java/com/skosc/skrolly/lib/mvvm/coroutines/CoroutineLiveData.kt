package com.skosc.skrolly.lib.mvvm.coroutines

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.skosc.skrolly.lib.mvvm.LiveEvent
import kotlinx.coroutines.*

fun <T> CoroutineScope.launchLiveData(
    dispatcher: CoroutineDispatcher = Dispatchers.IO,
    supplier: suspend () -> T
): LiveData<T> {
    val liveData = MutableLiveData<T>()

    launch(dispatcher) {
        liveData.postValue(supplier.invoke())
    }

    return liveData
}

fun <T> CoroutineScope.launchMediatorLiveData(
        dispatcher: CoroutineDispatcher = Dispatchers.IO,
        supplier: suspend () -> LiveData<T>
): LiveData<T> {
    val mediatorLiveData = MediatorLiveData<T>()

    launch(dispatcher) {
        val sourceLiveData = supplier.invoke()
        launch(Dispatchers.Main) {
            mediatorLiveData.addSource(sourceLiveData) {
                mediatorLiveData.postValue(it)
            }
        }
    }

    return mediatorLiveData
}

fun <T> CoroutineScope.launchLiveEvent(
    dispatcher: CoroutineDispatcher = Dispatchers.IO,
    action: suspend () -> T
): LiveEvent {
    val event = LiveEvent()

    launch(dispatcher) {
        action.invoke()
        event.fire()
    }

    return event
}