package com.skosc.skrolly.lib.mvvm

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope

open class BaseViewModel : ViewModel(), CoroutineScope by GlobalScope