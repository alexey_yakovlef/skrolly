package com.skosc.skrolly.lib.mvvm

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations

fun <T, F> LiveData<T>.map(transformer: (T) -> F): LiveData<F>
    = Transformations.map(this, transformer)